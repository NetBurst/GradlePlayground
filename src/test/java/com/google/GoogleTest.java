package com.google;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import selenoid.SelenoidSpawner;

import static org.testng.Assert.assertTrue;

public class GoogleTest extends SelenoidSpawner {
    private WebDriver driver;

    private String sessionName = "Google";
    private String browser = "chrome";
    private double browserVersion;
    private boolean enableVNC = true;
    private boolean enableVideo = false;

    @BeforeSuite
    public void setUp() {
        driver = spawnDriver(browser, browserVersion, enableVNC, enableVideo, sessionName);
    }

    @AfterSuite
    public void tearDown() {
    }

    @Test
    public void openHomePage() {
        HomePage homePage = new HomePage(driver);
        assertTrue(homePage.checkSanity(), "Start page is broken or didn't load correctly");
    }
}