package com.duckduckgo;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import selenoid.SelenoidSpawner;

import static org.testng.Assert.*;

public class HomePageTest extends SelenoidSpawner {
    private WebDriver driver;
    private HomePage homePage;
    private SearchResultsPage searchResultsPage;
    private SettingsPage settingsPage;

    private String sessionName = "DuckDuckGo";
    private String browser = "chrome";
    private double browserVersion;
    private boolean enableVNC = true;
    private boolean enableVideo = true;

    private String searchTerm = "Selenium";

    @BeforeSuite
    public void setUp() {
        driver = spawnDriver(browser, browserVersion, enableVNC, enableVideo, sessionName);
        homePage = new HomePage(driver);
    }

    @AfterSuite
    public void tearDown() {
        if (driver != null) driver.quit();
    }

    @BeforeGroups(groups = {"init", "serp", "settings"})
    public void navigateToHomePage() {
        homePage.openHomePage();
    }

    @Test(groups = "init")
    public void checkHomePage() {
        assertTrue(homePage.checkSanity());
        assertTrue(homePage.getTitle().contains("DuckDuckGo"), "Page title doesn't contain DuckDuckGo word!");
    }

    @Test(groups = "init", dependsOnMethods = "checkHomePage")
    public void checkAndClosePopupBadges() {
        assertTrue(homePage.checkMainPopupBadge(), "Main pop-up badge is missing on clean browser entry!");
        homePage.closePopupBadge();

        assertTrue(homePage.checkCookiePopupBadge(), "Cookie pop-up badge is missing on clean browser entry!");
        homePage.closePopupBadge();
    }

    @Test(groups = "init", dependsOnMethods = "checkAndClosePopupBadges")
    public void checkMenu() {
        homePage.openMenu();
        assertTrue(homePage.getMenu().isDisplayed(), "Menu is not visible!");
        homePage.closeMenu();
    }

    @Test(groups = "serp", dependsOnGroups = {"init"})
    public void testSearch() {
        searchResultsPage = homePage.search(searchTerm, true);
        assertEquals(searchResultsPage.getTitle(), searchTerm + " at DuckDuckGo");
    }

    @Test(groups = "serp", dependsOnMethods = {"testSearch"})
    public void checkSearchResults() {
        assertNotNull(searchResultsPage.getSearchResults(), "Search results are empty!");
    }

    @Test(groups = "serp", dependsOnMethods = {"checkSearchResults"})
    public void checkAdBlockVisibility() {
        assertTrue(searchResultsPage.getAdBlock().isDisplayed(), "Ad block is missing");
    }

    @Test(groups = "serp", dependsOnMethods = {"checkSearchResults"})
    public void checkSearchPageLazyLoad() {
        int numResultsBefore = searchResultsPage.getSearchResults().size();
        searchResultsPage.scrollToBottom();
        int numResultsAfter = searchResultsPage.getSearchResults().size();
        assertTrue(numResultsAfter > numResultsBefore, "Additional search results didn't load correctly!");
    }

    @Test(groups = "serp", dependsOnMethods = {"testSearch"})
    public void checkSearchFieldClearButton() {
        if (searchResultsPage.getSearchFieldText() != null) {
            assertTrue(searchResultsPage.checkSearchFieldClearButton(), "Search clear button isn't displayed!");
        }
    }

    @Test(groups = "serp", dependsOnMethods = {"checkSearchPageLazyLoad"})
    public void checkSeleniumHomePresentInSERP() {
        boolean present = searchResultsPage.getSearchResults().stream().anyMatch(element -> element
                .getAttribute("href").contains("http://docs.seleniumhq.org/"));
        assertTrue(present, "Selenium homepage link is missing in search results!");
    }

    @Test(groups = "serp", dependsOnMethods = "checkSearchFieldClearButton")
    public void clearSearchFieldWithButton() {
        searchResultsPage.clearSearchFieldWithButton();
        assertEquals(searchResultsPage.getSearchFieldText(), "", "Search field wasn't cleared!");
    }

    @Test(groups = "settings", dependsOnGroups = "init")
    public void openSettings() {
        homePage.openMenu();
        settingsPage = homePage.openSettings();
    }

    // TODO: test settings page.
}
