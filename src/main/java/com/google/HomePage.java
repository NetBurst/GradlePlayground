package com.google;

import org.openqa.selenium.WebDriver;

public class HomePage extends Google {
    public HomePage(WebDriver driver) {
        super(driver);
        driver.navigate().to("https://google.com");
    }

    public boolean checkSanity() {
        return true;
    }
}
