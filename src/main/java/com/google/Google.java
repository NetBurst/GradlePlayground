package com.google;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Google {
    protected WebDriver driver;

    public Google(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
