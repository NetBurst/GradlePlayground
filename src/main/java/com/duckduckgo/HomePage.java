package com.duckduckgo;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends DuckDuckGo {
    @FindBy(css = ".logo_homepage")
    private WebElement logo;
    @FindBy(css = "#search_form_input_homepage")
    private WebElement searchField;
    @FindBy(css = "#search_button_homepage")
    private WebElement searchButton;
    @FindBy(css = ".js-badge-main-msg")
    private WebElement popupBadgeMain;
    @FindBy(css = ".js-badge-cookie-msg")
    private WebElement popupBadgeCookie;
    @FindBy(css = "span.js-badge-link-dismiss")
    private WebElement popupBadgeMainCloseButton;
    @FindBy(css = "div.js-badge-cookie-msg span")
    private WebElement popupBadgeCookieCloseButton;
    @FindBy(css = "a.header__button--menu")
    private WebElement menuOpenButton;
    @FindBy(css = ".nav-menu__close")
    private WebElement menuCloseButton;
    @FindBy(css = "div.nav-menu--slideout")
    private WebElement menu;
    @FindBy(css = "a[href$=\"/settings\"]")
    private WebElement settingsLink;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage() {
        driver.navigate().to("https://duckduckgo.com");
    }

    public boolean checkSanity() {
        return logo.isDisplayed() && searchField.isDisplayed();
    }

    public void enterSearchTerm(String searchTerm) {
        searchField.sendKeys(searchTerm);
    }

    public SearchResultsPage performSearch(boolean useMouse) {
        if (useMouse) searchButton.click();
        else searchField.sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }

    public SearchResultsPage search(String searchTerm, boolean useMouse) {
        searchField.clear();
        searchField.sendKeys(searchTerm);
        if (useMouse) searchButton.click();
        else searchField.sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }

    public void openMenu() {
        menuOpenButton.click();
    }

    public WebElement getMenu() {
        return menu;
    }

    public void closeMenu() {
        menuCloseButton.click();
    }

    public SettingsPage openSettings() {
        settingsLink.click();
        return new SettingsPage(driver);
    }

    public boolean checkMainPopupBadge() {
        return popupBadgeMain.isDisplayed();
    }

    public boolean checkCookiePopupBadge() {
        return popupBadgeCookie.isDisplayed();
    }

    public void closePopupBadge() {
        if (checkMainPopupBadge()) {
            popupBadgeMainCloseButton.click();
        } else if (checkCookiePopupBadge()) {
            popupBadgeCookieCloseButton.click();
        }
    }
}
