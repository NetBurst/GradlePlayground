package com.duckduckgo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends DuckDuckGo {
    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".result.results_links_deep.highlight_d h2 > a.result__a")
    private List<WebElement> searchResults;

    @FindBy(css = "#ads")
    private WebElement adBlock;

    @FindBy(css = "#search_form_input_clear")
    private WebElement searchFieldClearButton;

    @FindBy(css = "#search_form_input")
    private WebElement searchField;

    public List<WebElement> getSearchResults() {
        return searchResults;
    }

    public String getSearchFieldText() {
        return searchField.getText();
    }

    public WebElement getAdBlock() {
        return adBlock;
    }

    public void clearSearchFieldWithButton() {
        scrollToTop();
        Actions actions = new Actions(driver);
        actions.moveToElement(searchField).perform();
        if (searchFieldClearButton != null) searchFieldClearButton.click();
    }

    public boolean checkSearchFieldClearButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(searchField).perform();
        return searchFieldClearButton.isDisplayed();
    }
}
