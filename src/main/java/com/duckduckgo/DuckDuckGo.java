package com.duckduckgo;

import framework.WebDriverBasic;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class DuckDuckGo extends WebDriverBasic {
    public DuckDuckGo(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
