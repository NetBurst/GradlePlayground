package selenoid;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SelenoidSpawner implements SelenoidInterface {
    public static WebDriver spawnDriver(String browser, double browserVersion, boolean enableVNC, boolean enableVideo, String sessionName) {
        if (browser.equals("chrome")) {
            ChromeOptions options = new ChromeOptions();
            if (browserVersion != 0.0) {
                options.setCapability("browserversion", browserVersion);
            }
            if (!sessionName.isEmpty()) {
                options.setCapability("name", sessionName);
            }
            options.setCapability("screenResolution", setScreenDefaults());
            options.setCapability("enableVNC", enableVNC);
            options.setCapability("enableVideo", enableVideo);
            return buildDriver(options);
        } else if (browser.equals("firefox")) {
            FirefoxOptions options = new FirefoxOptions();
            if (browserVersion != 0.0) {
                options.setCapability("browserversion", browserVersion);
            }
            options.setCapability("screenResolution", setScreenDefaults());
            options.setCapability("enableVNC", enableVNC);
            options.setCapability("enableVideo", enableVideo);
            return buildDriver(options);
        } else {
            System.out.println("Browser not supported");
        }
        return null;
    }

    private static WebDriver buildDriver(Capabilities options) {
        WebDriver driver = new RemoteWebDriver(setServerDefaults(), options);
        driver.manage().window().setSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    static URL setServerDefaults() {
        try {
            return new URL(PROTOCOL, HOST, PORT, PATH);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    static String setScreenDefaults() {
        return new StringBuilder()
                .append(SCREEN_WIDTH)
                .append("x")
                .append(SCREEN_HEIGHT)
                .append("x")
                .append(SCREEN_DEPTH)
                .toString();
    }

    static void setBrowserResolution(WebDriver driver, int screenWidth, int screenHeight) {
        driver.manage().window().setSize(new Dimension(screenWidth, screenHeight));
    }
}
