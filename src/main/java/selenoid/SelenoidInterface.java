package selenoid;

public interface SelenoidInterface {
    String PROTOCOL = "http";
    String HOST = "localhost";
    int PORT = 4444;
    String PATH = "/wd/hub";

    int SCREEN_WIDTH = 1920;
    int SCREEN_HEIGHT = 1200;
    int SCREEN_DEPTH = 24;
}
