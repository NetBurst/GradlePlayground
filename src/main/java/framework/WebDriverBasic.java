package framework;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class WebDriverBasic {
    protected WebDriver driver;

    /**
     * This simple method returns current page title. Simple as that.
     *
     * @return string of current page title.
     */
    public String getTitle() {
        return driver.getTitle();
    }

    /**
     * This method hovers cursor of current window into the centre of element. Page will be scrolled,
     * if said element is not in view. Scrolling is done to top-left corner of element's bounding box.
     *
     * @param element The WebElement into which center mouse cursor will be placed.
     *                Page will be scrolled to it, if element is not in view.
     */
    public void scrollToElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }

    /**
     * This method scrolls viewport vertically to current bottom of page. Wording is important here,
     * because during scrolling current bottom can move further, because of lazy loading of elements.
     * This method doesn't compensate for that.
     */
    // TODO: need to make scrolling wait procedure more adequate and graceful
    public void scrollToBottom() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.documentElement.scrollHeight);");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method scrolls viewport vertically to topmost position.
     */
    public void scrollToTop() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 0);");
    }
}
